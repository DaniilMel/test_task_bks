import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";
import VueAxios from "vue-axios";

Vue.use(Vuex);
Vue.use(VueAxios, axios);

export default new Vuex.Store({
  state: {
    products: [],
  },
  getters: {
    getProducts(state) { return state.products; }
  },
  mutations: {
    setProducts(state, data) {
      state.products = data;
    }
  },
  actions: {
    loadProducts(context) {
      Vue.axios.get('./products.json')
        .then((res) => {
          context.commit('setProducts',res.data);
        }).catch((err) => {
          console.log(err);
        })
    }
  },
  modules: {
  }
})
